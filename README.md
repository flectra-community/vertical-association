# Flectra Community / vertical-association

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[membership_variable_period](membership_variable_period/) | 2.0.1.0.0| Variable period for memberships
[membership_prorate_variable_period](membership_prorate_variable_period/) | 2.0.1.0.0| Prorate membership fee for variable periods
[contract_membership_delegated_partner](contract_membership_delegated_partner/) | 2.0.1.0.0| Set delegate membership on the contract
[membership_initial_fee](membership_initial_fee/) | 2.0.1.0.0| Initial fee for memberships
[membership_withdrawal](membership_withdrawal/) | 2.0.1.0.0| Log membership withdrawal reason and date of request
[membership_extension](membership_extension/) | 2.0.1.2.0| Improves user experience of membership addon
[membership_delegated_partner](membership_delegated_partner/) | 2.0.1.1.0| Delegate membership on a specific partner
[membership_prorate](membership_prorate/) | 2.0.1.0.0| Prorate membership fee


