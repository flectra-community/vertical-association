# Copyright 2015 Tecnativa - Pedro M. Baeza
# Copyright 2017 Tecnativa - David Vidal
# License AGPL-3 - See https://www.gnu.org/licenses/agpl-3.0

{
    "name": "Initial fee for memberships",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "category": "Association",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/vertical-association",
    "depends": ["membership"],
    "data": ["views/product_template_views.xml"],
}
